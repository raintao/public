#!/usr/bin/env sh

# 确保脚本抛出遇到的错误
set -e


# 如果是发布到自定义域名
# echo 'www.example.com' > CNAME

# git init
git add -A
git commit -m '更新'

# 如果发布到 https://<USERNAME>.github.io  USERNAME=你的用户名 

git push -f git@e.coding.net:raintao/public.git master
git push -f git@gitee.com:raintao/public.git master

# 如果发布到 https://<USERNAME>.github.io/<REPO>  REPO=github上的项目
# git push -f git@github.com:<USERNAME>/<REPO>.git master:gh-pages

